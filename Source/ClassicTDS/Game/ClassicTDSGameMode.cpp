// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ClassicTDSGameMode.h"
#include "ClassicTDSPlayerController.h"
#include "../Character/ClassicTDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AClassicTDSGameMode::AClassicTDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AClassicTDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}