// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClassicTDSGameMode.generated.h"

UCLASS(minimalapi)
class AClassicTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AClassicTDSGameMode();
};



