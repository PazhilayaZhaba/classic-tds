// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ClassicTDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ClassicTDS, "ClassicTDS" );

DEFINE_LOG_CATEGORY(LogClassicTDS)
 